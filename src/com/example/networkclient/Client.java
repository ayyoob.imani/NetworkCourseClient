package com.example.networkclient;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.util.ArrayList;

public class Client implements Runnable {

	@Override
	public void run() {
		nextSeqNom = 0;
		base = 0;
		while(true){
			if(mustSendPacket()){
				sendPacket(packets.get(nextSeqNom), serverAddress , nextSeqNom , clientName);
			}
		}

	}

	
	private int winSize;
	private int base;
	private ArrayList<String> packets;
	private DatagramSocket socket  = null;
	private static int nextSeqNom ;

	private static InetSocketAddress serverAddress;
	private static String clientName;
	/**
	 * 
	 * @param data
	 * @return 
	 */
	
	public static boolean mustSendPacket(){
		//TODO
		return true;
	}
	
	public synchronized void setData(String data) {
		String tempData = null;
		while(data.length() > 250 )
		{
			tempData = data.substring(0,250);
			packets.add(tempData);
			data = data.substring(250);
		}
		
		if(data.length()>0){
			packets.add(data);
		}	
		
	}

	/**
	 * 
	 * @param packet
	 * @return 
	 */
	private synchronized void sendPacket(String packet ,InetSocketAddress socketAddress, int seqNom, String clientName) {
		int nameLen = clientName.length();
		if( nameLen > 7 ){
			clientName = clientName.substring(0,7);
		}
		else if(nameLen < 7 ){
			for(int i = nameLen ; i < 7 ; i++){
				clientName += " ";
			}
		}
		byte bSeqNom = (byte) seqNom;
		byte bSize = (byte) packet.length();
		String sendString = clientName + bSeqNom + bSize + packet;
		
		byte[] sendData = sendString.getBytes();
		
		DatagramPacket sendPacket = null;
		try {
			 sendPacket = new DatagramPacket(sendData, sendData.length , socketAddress);
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			socket.send(sendPacket);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @return 
	 */
	private String rcvPckt() {
		throw new UnsupportedOperationException();
	}

	



}
